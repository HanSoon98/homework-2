// functions2.rs
// Make me compile! Scroll down for hints :)

#[test]
fn main() {
    call_me(3);
}
#[allow(dead_code)]
fn call_me(num: u32) {
    for i in 0..num {
        println!("Ring! Call number {}", i + 1);
    }
}




























// Rust requires that all parts of a function's signature have type annotations,
// but `call_me` is missing the type annotation of `num`.
