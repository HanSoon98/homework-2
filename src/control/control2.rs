// control2.rs
// Make me compile! Scroll down for hints :)

//I came up with a solution, but I thought that it was very repetitive, so I exersised the DRY principle to the best of my ability. 
//I rewrote the main fn and wrote two other fns to fulfill the same functionality as the original. 
//I commented out my original solution at the bottom.

#[derive(Debug)]
enum Colors {
  Red,
  Green,
  Blue,
  Orange,
  Yellow,
}

#[test]
fn main() {
  for x in 0..5{
    get_color(x);
  }
}

#[allow(dead_code)]
fn get_color(x: u16){
  match x {
    0 => print(Colors::Red),
    1 => print(Colors::Green),
    2 => print(Colors::Yellow),
    3 => print(Colors::Blue),
    4 => print(Colors::Orange),
    _ => println!("Color Not Found!")
  };
}

fn print(color: Colors){
  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };
  println!("{:?}s are {:?}", fruit, color);
}

//the following is my first/original solution

/*
fn main() {
  let color = Colors::Red;

  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };

  println!("{:?}s are {:?}", fruit, color);

  let color = Colors::Green;

  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };
  println!("{:?}s are {:?}", fruit, color);

  let color = Colors::Yellow;

  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };
  println!("{:?}s are {:?}", fruit, color);

  let color = Colors::Blue;

  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };
  println!("{:?}s are {:?}", fruit, color);

  let color = Colors::Orange;

  let fruit = match color {
    Colors::Red => "apple",
    Colors::Green => "grape",
    Colors::Yellow => "banana",
    Colors::Blue => "blueberry", 
    Colors::Orange => "orange"
  };
  println!("{:?}s are {:?}", fruit, color);
}
*/


// The match expression is wrong is a couple ways. 
// First, it doesn't consider all the possible choices for color, which is of type Colors. Fix this by either using a wildcard pattern, or match each of the other possibilities for the enum.
// Second, one of the match arms does not match the type of the other arms. Make it match.
// Third, there is no semicolon at the end;